﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using TMPro;
using UnityEngine;

public class StatManager : MonoBehaviour
{
    public bool stop;
    
    [Header("Resource")] 
    public int rAmount;
    public string rName = "resource";
    public float rTime = 10f;
    public int rGain = 1;

    public GameObject rBarObject;
    private Transform rBar;
    public TextMeshProUGUI rNameUI;
    public TextMeshProUGUI rTimeLeftUI;
    public TextMeshProUGUI rCountUI;
    public GameObject rFastBarObject;
    private FastBarScroll rFastBarScroll;
    
    private float rTimeLeft;
    private float speedRMultiplier = 1;

    [Header("Colon")] 
    public int cAmount;
    public string cName = "colon";
    public float cTime = 10f;
    public int cGain = 1;

    public GameObject cBarObject;
    private Transform cBar;
    
    public TextMeshProUGUI cNameUI;
    public TextMeshProUGUI cTimeLeftUI;
    public TextMeshProUGUI cCountUI;
    public GameObject cFastBarObject;
    private FastBarScroll cFastBarScroll;
    
    private float cTimeLeft;
    private float speedCMultiplier = 1;

    [Header("Tap")] 
    public float timeRemoveInSec;
    private float clickEfficiencyMultiplier = 1;
    
    private InputMaster controls;

    private void Awake()
    {
        controls = new InputMaster();
        controls.Game.Tap.performed += tap => Tap();
    }

    void Start()
    {
        rBar = rBarObject.transform;
        cBar = cBarObject.transform;

        rFastBarScroll = rFastBarObject.GetComponent<FastBarScroll>();
        cFastBarScroll = cFastBarObject.GetComponent<FastBarScroll>();
        
        rTimeLeft = rTime;
        cTimeLeft = cTime;
        
        UpdateUI();
    }
    void Update()
    {
        if (!stop)
        {
            // Advance bar for 1 tick
            ProgressBar();
        
            // Update UI
            UpdateUI();
        }
    }

    void ProgressBar()
    {
        /* RESOURCE */
        rTimeLeft -= Time.deltaTime * speedRMultiplier;
        if (rTimeLeft <= 0)
        {
            GiveResource(rGain);
            rTimeLeft = rTime + rTimeLeft;
        }
        
        /* COLON */
        cTimeLeft -= Time.deltaTime * speedCMultiplier;
        if (cTimeLeft <= 0)
        {
            GiveColon(cGain);
            cTimeLeft = cTime + cTimeLeft;
        }
    }
    
    void UpdateUI()
    {
        /* RESOURCE */
        if (rTime < .06f)
        {
            if (rBarObject.activeInHierarchy) rBarObject.SetActive(false);
            if (!rFastBarObject.activeInHierarchy) rFastBarObject.SetActive(true);
        }
        else
        {
            if (!rBarObject.activeInHierarchy) rBarObject.SetActive(true);
            if (rFastBarObject.activeInHierarchy) rFastBarObject.SetActive(false);

            rBar.localScale = new Vector3(1-(rTimeLeft / rTime), rBar.localScale.y, rBar.localScale.z);
        }
            
        rNameUI.text = rGain + " " + rName;
        rTimeLeftUI.text = Mathf.FloorToInt(rTimeLeft) + "s";
        rCountUI.text = rAmount + " " + rName;
        
        
        /* COLON */
        if (cTime < .06f)
        {
            if (cBarObject.activeInHierarchy) cBarObject.SetActive(false);
            if (!cFastBarObject.activeInHierarchy) cFastBarObject.SetActive(true);
        }
        else
        {
            if (!cBarObject.activeInHierarchy) cBarObject.SetActive(true);
            if (cFastBarObject.activeInHierarchy) cFastBarObject.SetActive(false);

            cBar.localScale = new Vector3(1-(cTimeLeft / cTime), cBar.localScale.y, cBar.localScale.z);
        }
        
        cNameUI.text = cGain + " " + cName;
        cTimeLeftUI.text = Mathf.FloorToInt(cTimeLeft) + "s";
        cCountUI.text = cAmount + " " + cName;
    }

    /* Tap PART */
    void Tap()
    {
        // Remove the correct amount of time and calculate gain if its more than the rTime
        
        float rTimeRemoved = timeRemoveInSec * clickEfficiencyMultiplier;

        Debug.Log(rTimeRemoved);
        
        rTimeLeft = rTimeLeft - rTimeRemoved;
        if (rTimeLeft <= 0)
        {
            int rAmountToAdd = Mathf.FloorToInt(-rTimeLeft / rTime) + 1;
            float remain = (1 - (-rTimeLeft / rTime) - (rAmountToAdd-1)) * rTime;
            
            Debug.Log("ToAdd : " + rAmountToAdd + "   remain : " + remain);

            GiveResource(rAmountToAdd);
            rTimeLeft = remain;
        }
    }

    public void GiveResource(int amount)
    {
        rAmount += amount;
        
    }
    public void GiveColon(int amount)
    {
        cAmount += amount;
        
    }

    private void OnEnable()
    {
        controls.Enable();
    }
    private void OnDisable()
    {
        controls.Disable();
    }
}
