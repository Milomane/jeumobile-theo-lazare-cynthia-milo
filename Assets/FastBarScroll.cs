﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FastBarScroll : MonoBehaviour
{
    public Image image;
    private Material imageMaterial;

    public float scrollSpeed = 10;
    
    void Start()
    {
        imageMaterial = image.material;
    }

    // Update is called once per frame
    void Update()
    {
        imageMaterial.SetTextureOffset("_MainTex", imageMaterial.GetTextureOffset("_MainTex") - new Vector2(scrollSpeed * Time.deltaTime, 0));
        if (imageMaterial.mainTextureOffset.x <= -1)
        {
            imageMaterial.SetTextureOffset("_MainTex", new Vector2(imageMaterial.mainTextureOffset.x + 1, 0));
        }
    }
}